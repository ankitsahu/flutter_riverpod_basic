import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_riverpod_basic/main.dart';

class Home extends ConsumerWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final int counter = ref.watch(counterProvider);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Riverpod"),
        actions: [
          IconButton(
            icon: const Icon(Icons.do_not_disturb_on_total_silence_outlined),
            onPressed: () {
              ref.refresh(counterProvider);
            },
          ),
        ],
      ),
      body: Center(
        child: Text(
          "$counter",
          style: const TextStyle(fontSize: 200),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            ref.read(counterProvider.notifier).state++;
          },
          icon: const Icon(Icons.arrow_upward),
          label: const Text("Increase")),
    );
  }
}
